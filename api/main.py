from fastapi import FastAPI

import uvicorn
import requests

app = FastAPI()


# Pour l'accueil du webservice
@app.get("/")
def callApi():
    return {"Welcome to my Api !!!"}


deckId = ''
remaining = 0


# Pour initialiser un deck
@app.get("/creer-un-deck")
def createDeck():
    global deckId
    global remaining
    response = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    
    deckId = response.json()["deck_id"]
    remaining = response.json()["remaining"]

    return deckId


# Pour tirer les cartes
@app.get("/cartes/{nb_cards}")
def drawCards(nb_cards:int):

    global deckId
    global remaining

    if (nb_cards < 0 or nb_cards > 52): # Renvoie un message d'erreur si le nombre de cartes n'est pas entre 0 et 52
        return {'error': 'The number of cards you must draw should be a non negative integer less than or equal to 52 !!!'}
    else:
        if deckId == "": # Initialise un nouveau deck et tire les cartes si aucun deck n'a été initialisé 
            response = requests.get("https://deckofcardsapi.com/api/deck/new/draw/?count={}".format(nb_cards))
            deckId = response.json()["deck_id"]

        elif (remaining < nb_cards): # Remélange les cartes avant tirage s'il n'y a pas assez de cartes dans le deck
            response = requests.get("https://deckofcardsapi.com/api/deck/{}/shuffle/".format(deckId))
            response = requests.get("https://deckofcardsapi.com/api/deck/{}/draw/?count={}".format(deckId, nb_cards))

        else:
            response = requests.get("https://deckofcardsapi.com/api/deck/{}/draw/?count={}".format(deckId, nb_cards))

        response = response.json()
        remaining = response["remaining"]
        return {"deck_id": deckId, 'remaining': remaining, "cards": response["cards"]}

if __name__ == "__main__":

    uvicorn.run(app, host="127.0.0.1", port=8000)