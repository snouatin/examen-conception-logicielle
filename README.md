# Examen Conception Logicielle

Le présent dépôt propose deux applictions: un **webservice** qui repose sur l'utilisation de l'api documentée ici: https://deckofcardsapi.com/ et un **client** de ce webservice.

## Organisation du dépôt

Le dépôt est organisé en deux dossiers : `api` et `client`.

### Webservice

Le webservice est contenu dans le dossier `api`. On y retrouve deux principaux fichiers: `requirements.txt` et `main.py`.

+ `requirements.txt` contient la liste des librairies nécessaires à installer avant l'utilisation du webservice.
+ `main.py` est le fichier lançant le webservice.

Le webservice récupère un paquet de cartes (deck) de l'api et renvoie son id. Il tire aussi des cartes du deck parmi les restantes.

### Client

Le client est contenu dans le dossier `client`. Ce dossier contient les fichiers: `requirements.txt`, `functions.py`, `tests.py`, `scenario.py` et `main.py`.

+ `requirements.txt` contient la liste des librairies nécessaires à installer avant l'utilisation du client.
+ `functions.py` est le fichier dans lequel sont définis les fonctions principales du client. On y retrouve deux classes: `Card` qui définit la construction et la représentation des cartes puis `ClientActions` dans laquelle  sont définies trois fonctions qui representent les actions que peut réaliser le client.
+ `tests.py` réalise deux tests unitaires sur la fonction de comptage de cartes.
+ `scenario.py` initialise un deck, tire 10 cartes et compte le nombre de cartes tirées de chaque couleur.
+ `main.py` contient une classe `WelcomeView` qui permet à tout utilisateur d'interagir avec le client.

## Quick Start

Pour expérimenter ce webservice et le client, veuillez cloner ce dépôt.

1. **Pour accéder et lancer le webservice**, exécutez les lignes de commandes suivantes:

    * `cd examen-conception-logicielle/api/` pour accéder au dossier `api`.
    * `pip install -r requirements.txt` ou `pip3 install -r requirements.txt` pour installer les librairies nécessaires.
    * `python main.py` ou `python3 main.py` pour lancer le webservice.

Une fois lancé, le webservice est accessible au : http://127.0.0.1:8000/. Voici la liste des requêtes utiles:

+ `http://127.0.0.1:8000/creer-un-deck` pour initialiser un deck. Il renvoie l'id du deck créer.
+ `http://127.0.0.1:8000/cartes/3` pour tirer 3 cartes d'un deck. Vous pouvez modifier le nombre de cartes à tirer en remplaçant le chiffre 3 par un nombre de votre choix.

2. **Pour accéder et lancer le client**, il faut au préalable lancer le webservice. Ensuite, exécutez les lignes de commandes suivantes dans un nouveau terminal:

    * `cd examen-conception-logicielle/client` pour accéder au dossier `client`.
    * `pip install -r requirements.txt` ou `pip3 install -r requirements.txt` pour installer les librairies nécessaires.
    * Pour lancer le scenario, exécutez `python scenario.py` ou `python3 scenario.py`.
    * Pour interagir avec le client, exécutez `python main.py` ou `python3 main.py`.
    * Pour lancer les tests implémentés, exécutez `python tests.py` ou `python3 tests.py`.
