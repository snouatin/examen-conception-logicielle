from functions import ClientActions
from PyInquirer import prompt


class WelcomeView:

    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'choice',
                'message': 'What do you want to do now ?',
                'choices': ['1. Create a  new decker !'
                            , '2. Draw some cards'
                            , '3. Exit !']
            }
        ]
        self.status = True

    def make_choice(self):

        reponse = prompt(self.questions)
        print('')
        if reponse['choice'] == '1. Create a  new decker !':
            print('')
            print("======> Initializing the deck...")
            print('')
            print('The ID of the new deck is : ' + ClientActions.createDeck())
            print('')

        elif reponse['choice'] == '2. Draw some cards':

            print('')
            print("How much ? (integer between 1 and 52)")
            nb_cards = int(input())
            while(nb_cards > 52 or nb_cards < 1):
                print('')
                print("Please, enter an integer between 1 and 52 !!!")
                nb_cards = int(input())

            print('')
            print("======> Drawing {} cards...".format(nb_cards))
            r = ClientActions.drawCards(nb_cards)
            print('')
            print('The cards drawed are :')
            j = 0
            for i in r:
                j += 1
                print('Card {}:'.format(j), i)
                print('')

            print("You can count how much cards of every suit, you have.")
            print("Do you want to count them ? (y/n)")
            answer = input()
            if answer == "y":
                print('')
                print("======> Grouping the cards by suit...")
                print('')
                print('Summary :', ClientActions.countCards(r))
                print('')

        elif reponse['choice'] == '3. Exit !':
            print('')
            print('******************************************************')
            print('************* Goodbye!!! See you soon... *************')
            print('******************************************************')
            print('')
            self.status = False


if __name__ == "__main__":

    print('')
    print("#######################################################")
    print("############ Welcome to my application !!! ############")
    print("################ Enjoy your journey... ################")
    print("#######################################################")
    print('')

    execution = WelcomeView()
    while(execution.status):
        execution.make_choice()
