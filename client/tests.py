import pytest
from functions import ClientActions, Card


def test1():

    data = [
        Card('https://deckofcardsapi.com/static/img/0C.png', '10', 'CLUBS','0C'),
        Card('https://deckofcardsapi.com/static/img/JS.png', 'JACK','SPADES', 'JS'),
        Card('https://deckofcardsapi.com/static/img/KS.png', 'KING', 'SPADES', 'KS'),
        Card('https://deckofcardsapi.com/static/img/AC.png', 'ACE', 'CLUBS', 'AC'),
        Card('https://deckofcardsapi.com/static/img/6C.png', '6', 'CLUBS', '6C'),
        Card('https://deckofcardsapi.com/static/img/JH.png', 'JACK', 'HEARTS', 'JH')
        ]

    expected = {'H': 1, 'S': 2, 'D': 0, 'C': 3}
    assert ClientActions.countCards(data) == expected


def test2():

    data = [
        Card('https://deckofcardsapi.com/static/img/0C.png', '10', 'CLUBS','0C'),
        Card('https://deckofcardsapi.com/static/img/JS.png', 'JACK','SPADES', 'JS'),
        Card('https://deckofcardsapi.com/static/img/KS.png', 'KING', 'SPADES', 'KS'),
        Card('https://deckofcardsapi.com/static/img/JH.png', 'JACK', 'HEARTS', 'JH')
        ]
    expected = {'H': 1, 'S': 1, 'D': 1, 'C': 1}
    assert ClientActions.countCards(data) != expected

if __name__ == "__main__":

    test1()
    test2()