import requests


serveur = "http://127.0.0.1:8000"


class Card(object):

    def __init__(self, image: str, value: str, suit: str, code: str):
        self.image = image
        self.value = value
        self.suit = suit
        self.code = code

    def __repr__(self):
        return '{"image:" %s, "value": %s, "suit": %s, "code": %s}' % (self.image, self.value, self.suit, self.code)


class ClientActions:
    def createDeck():
        return requests.get(serveur + "/creer-un-deck").json()


    def drawCards(nb_cards: int):
        response = requests.get(serveur + "/cartes/{}".format(nb_cards)).json()
        return [Card(card['image'], card['value'], card['suit'], card['code']) for card in response['cards']]


    def countCards(cards):
        result = {"H": 0, "S": 0, "D": 0, "C": 0}
        for card in cards:
            result[card.suit[0]] += 1
        return result
