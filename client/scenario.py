from functions import ClientActions

if __name__ == "__main__":

    print('')
    print("#################################################")
    print("############ Welcome to the scenario ############")
    print("#################################################")

    print('')
    print("======> Initializing the deck...")
    id = ClientActions.createDeck()
    print('')
    print('The ID of the new deck is : ' + id)

    print('')
    print("======> Drawing ten cards from the deck n° " + id + ' ...')
    r = ClientActions.drawCards(10)
    print('')
    print('The cards drawed are :')
    j = 0
    for i in r:
        j += 1
        print('Card {}:'.format(j), i)
        print('')
    
    print("======> Grouping the cards by suit...")
    print('')
    print('Summary :', ClientActions.countCards(r))
    print('')
